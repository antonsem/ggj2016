using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoulBar : MonoBehaviour {

    public RectTransform souls;
    public Text soulsText;

    public int maxSouls = 100;
    public int currentSouls = 0;

    void Start()
    {
        SetSoulsText();
    }

    public void SetSoulsBar(int _soulAmount)
    {
        //1 soul = 0.01 scale - max souls = 1 scale
        if (_soulAmount <= maxSouls)
        {
            currentSouls = _soulAmount;
            souls.localScale = new Vector3((float)(_soulAmount) / (float)maxSouls, 1, 1);
            SetSoulsText();
        }
    }

    public void SetMaxSouls(int _maxSouls)
    {
        maxSouls = _maxSouls;
        SetSoulsText();
    }

    public void SetSoulsText()
    {
        soulsText.text = currentSouls + " / " + maxSouls + " MAX SOUL";
    }

    public void SetAll(int _soulAmount, int _maxSouls)
    {
        SetSoulsText();
        SetSoulsBar(_soulAmount);
        SetMaxSouls(_maxSouls);
    }


}
