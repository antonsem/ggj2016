using UnityEngine;
using System.Collections;

public class Soul : Entity
{
    private DataManager dataManager;
    private Player player;
    private float pickCooldown = 0;
    private float timeOut = 30;

    private bool isLerping;
    private bool isInteractable;
    private float timeStartedToLerping;
    private Vector3 startVector;
    private Vector3 endVector;

    public int soulValue;

    public float timeTakenDuringLerp = 0.1f;

    void Start()
    {
        base.Start();
        dataManager = DataManager.Instance;
        player = FindObjectOfType<Player>();
        GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-200f, 200f), 200, Random.Range(-200f, 200f)));
        pickCooldown = Time.realtimeSinceStartup;
        isInteractable = true;
    }


    public override void Interact()
    {
        if (Time.realtimeSinceStartup - pickCooldown > 1 && isInteractable)
        {
            SoundHelper.Instance.PlaySound(interactionSounds[0], 0.2f, 0.2f);
            dataManager.SoulCount += soulValue;
            dataManager.CountSummonSouls(soulValue);
            player.clearList.Add(this);
        }
    }

    public void CollectorInteract()
    {
        if (Time.realtimeSinceStartup - pickCooldown > 1)
        {
            //endVector = FindObjectOfType<Player>().transform.position;
            startVector = transform.position;
            StartLerping();
            GetComponent<Collider>().isTrigger = true;
            isInteractable = false;
        }
    }

    void Update()
    {
        timeOut -= Time.deltaTime;
        transform.LookAt(Camera.main.transform.position, -Vector3.up);
        if (timeOut < 0)
            player.clearList.Add(this);
    }

    void FixedUpdate()
    {
        if (isLerping)
        {
            float timeSinceStarted = Time.time - timeStartedToLerping;
            float percentageComplete = timeSinceStarted / timeTakenDuringLerp;

            transform.position = Vector3.Lerp(startVector, new Vector3(FindObjectOfType<Player>().transform.position.x, 1.25f, FindObjectOfType<Player>().transform.position.z), percentageComplete);
            if (percentageComplete >= 1f)
            {
                isLerping = false;
                dataManager.SoulCount += soulValue;
                dataManager.CountSummonSouls(soulValue);
                player.clearList.Add(this);
            }
        }
    }

    void StartLerping()
    {
        isLerping = true;
        timeStartedToLerping = Time.time;
    }
}
