using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public static class UIManager
{
    private static bool isUIEnabled;

    private static Player player;
    private static List<GameObject> obeliskPosList = new List<GameObject>();

    public static GameObject unitSpawner;

    private static string tempType;
    private static string[] vendorCache;
    private static Color colorCache; 
    private static Color highlightColor = new Color(.18f,.6f,.79f,1f);
 
    private static int selectedIndex = 0; 
    public static Transform currentItem;
    
    public static string[] playerPanels = {
        "Speed",
        "Radius",
        "Max Souls",
        "Increase Income"
    };

    public static string[] minionPanels = {
        "Buy Collector",
        "Buy Mason",
        "Minion Speed",
        "Collector Radius"
    };
    
    public static string[] obeliskPanels = {
        "Buy Obelisk",
        "Overload",
        "Big Souls"
    };

    public static Transform statsPanel;
    public static GameObject upgradePanel;
    private static GameObject gameUICanvas;

    private static Text soulAmount;
    private static Text collectorCount;
    private static Text masonCount;
    private static Text incomeCount;
    private static Text obeliskCount;
    
    private static DataManager dataManager;
    
    private static Button closePanelButton;
    
    public static bool isPanelOpened;
    
    public static int SoulAmount
    {
        get { return int.Parse(soulAmount.text); }
        set { soulAmount.text = value.ToString(); }
    }

    public static int CollectorCount
    {
        get { return int.Parse(collectorCount.text); }
        set { collectorCount.text = value.ToString(); }
    }

    public static int MasonCount
    {
        get { return int.Parse(masonCount.text); }
        set { masonCount.text = value.ToString(); }
    }

    public static int IncomeCount
    {
        get { return int.Parse(incomeCount.text); }
        set { incomeCount.text = value.ToString(); }
    }

    public static int ObeliskCount
    {
        get { return int.Parse(obeliskCount.text); }
        set { obeliskCount.text = value.ToString(); }
    }

    public static float verticalSpacing = 60f;
    public static float verticalOffset = 100f;
    public static float leftOffset = 30f;
    
    public static float messagePadding = 105f;

    public static void Init()
    {
        gameUICanvas = GameObject.Find("GameUI");

        soulAmount = GameObject.Find("SoulsAmount").GetComponent<Text>();
        collectorCount = GameObject.Find("CollectorAmount").GetComponent<Text>();
        masonCount = GameObject.Find("MasonAmount").GetComponent<Text>();
        incomeCount = GameObject.Find("IncomeAmount").GetComponent<Text>();
        obeliskCount = GameObject.Find("ObeliskAmount").GetComponent<Text>();
        
        closePanelButton = GameObject.Find("CloseButton").GetComponent<Button>();
        closePanelButton.onClick.AddListener(() => {
            CloseUpgradePanel();
         });

        upgradePanel = GameObject.FindWithTag("UpgradePanel");
        unitSpawner = GameObject.FindWithTag("UnitSpawner");
        player = GameObject.FindObjectOfType<Player>();
        dataManager = DataManager.Instance;

        foreach (GameObject g in GameObject.FindGameObjectsWithTag("ObeliskPos"))
        {
            obeliskPosList.Add(g);
        }

        isUIEnabled = true;

        CloseUpgradePanel();

    }
    
    public static void CloseUpgradePanel(){

        if (isUIEnabled)
        {
            foreach (Transform child in upgradePanel.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
            closePanelButton.gameObject.SetActive(false);
            upgradePanel.SetActive(!upgradePanel.activeSelf);
            player.canMove = !upgradePanel.activeSelf;
            isPanelOpened = false;
            selectedIndex = 0;
            currentItem = null;
            isUIEnabled = false;
        }

    }
    
    public static void OpenUpgradePanel(string[] arr){
        
        selectedIndex = 0;
        vendorCache = arr;
        
        for (int i = 0; i < arr.Length; i++)
        {
            GameObject panel = ResourceManager.GetPanelItem(upgradePanel.transform.position);
            RectTransform rc = panel.GetComponent<RectTransform>();
            rc.SetParent(upgradePanel.transform);

            panel.gameObject.name = ParsePanelName(arr[i]);
            panel.transform.Find("Explanation").GetComponent<Text>().text = arr[i];

            Button increase_btn = panel.transform.Find("IncreaseBtn").GetComponent<Button>();

            string panelName = arr[i];
            increase_btn.onClick.AddListener(() => { IncreaseUpgrade(panelName); });

            rc.localScale = new Vector3(1f, 1f, 1f);

            Vector3 rcPos = rc.localPosition;
            rcPos.x = leftOffset;
            rcPos.y -= i * verticalSpacing - verticalOffset;

            rc.localPosition = rcPos;
            isPanelOpened = true;
        }
        
        currentItem = upgradePanel.transform.GetChild(0);
        colorCache = currentItem.GetComponent<Image>().color; 
        currentItem.GetComponent<Image>().color = highlightColor;
        upgradePanel.GetComponent<Image>().color = highlightColor;
        
        RectTransform buttonRect = closePanelButton.GetComponent<RectTransform>();
        
        buttonRect.anchoredPosition = new Vector2(
            upgradePanel.GetComponent<RectTransform>().sizeDelta.x/2,
            upgradePanel.GetComponent<RectTransform>().sizeDelta.y/2
        );
        
    }

    public static void ToggleUpgradePanel(string vendorType)
    {
        
        switch(vendorType){
            case "player":
                OpenUpgradePanel(playerPanels);
            break;
            case "minion":
                OpenUpgradePanel(minionPanels);
            break;
            case "obelisk":
                OpenUpgradePanel(obeliskPanels);
            break;
        }
        
        upgradePanel.SetActive(!upgradePanel.activeSelf);
        player.canMove = !upgradePanel.activeSelf;
        
        if(!upgradePanel.activeSelf){
            dataManager.StartCoroutine(FadeTo(upgradePanel.GetComponent<Image>(),0f,.25f));
            closePanelButton.gameObject.SetActive(false);
            
            foreach (Transform child in upgradePanel.transform) {
                GameObject.Destroy(child.gameObject);
            }
            
        } else {
            dataManager.StartCoroutine(FadeTo(upgradePanel.GetComponent<Image>(),.3f,.25f));
            closePanelButton.gameObject.SetActive(true);
        }

        tempType = vendorType;

        SetPrices(vendorType);

        isUIEnabled = true;        

    }
    
    public static IEnumerator FadeTo(Image img, float aValue, float aTime)
    {
        float alpha = img.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(.18f,.6f,.79f, Mathf.Lerp(alpha, aValue, t));
            img.color = newColor;
            yield return null;

        }
    }
    
     public static IEnumerator MessageFadeTo(RectTransform messagePanel, float aValue, float aTime)
    {
        Image img = messagePanel.GetComponent<Image>();
        
        float alpha = img.color.a;
        float t = -3f;
        
        while(t < 1.0f){
            
            t += Time.deltaTime / aTime;
            Color newColor = new Color(.18f,.6f,.79f, Mathf.Lerp(alpha, aValue, t));
            img.color = newColor;
            
            if(t>0){
                messagePanel.transform.Find("MessageImage").GetComponent<Image>().color = newColor;
                messagePanel.transform.Find("MessageText").gameObject.SetActive(false);
            } 
            
            yield return null;
        }
    }

    public static void ShowMessagePanel(string message){
        GameObject messagePanel = ResourceManager.GetMessageItem(gameUICanvas.transform.position);
        RectTransform rc = messagePanel.GetComponent<RectTransform>();
        rc.SetParent(gameUICanvas.transform);

        messagePanel.gameObject.name = "MessagePanel";
        messagePanel.transform.Find("MessageText").GetComponent<Text>().text = message;

        Button closeMessagePanelButton = messagePanel.transform.Find("CloseMessageButton").GetComponent<Button>();
        closeMessagePanelButton.onClick.AddListener(() => { GameObject.Destroy(messagePanel.gameObject); });

        rc.localScale = new Vector3(1f, 1f, 1f);

        Vector3 rcPos = rc.localPosition;
        // rcPos.x = Screen.width - (rc.sizeDelta.x / 2) + messagePadding;
        // rcPos.y -= Screen.height - (rc.sizeDelta.y / 2);

        rc.localPosition = rcPos;
        
        dataManager.StartCoroutine(MessageFadeTo(messagePanel.GetComponent<RectTransform>(), 0f, 1f));
    }
    
    public static void TurnOffUpgradePanel()
    {
        upgradePanel.SetActive(false);
        player.canMove = true;
    }
    
   

    public static void IncreaseUpgrade(string panelName)
    {
        string parsedName = panelName.Replace(" ", "") + "Panel";
        Text textNode = upgradePanel.transform.Find(parsedName).transform.Find("Count").GetComponent<Text>();

        if (int.Parse(dataManager.GetPrice(panelName)) <= dataManager.SoulCount)
        {
            dataManager.DecreaseSouls(int.Parse(dataManager.GetPrice(panelName)));
            SoundHelper.Instance.PlaySound(SoundHelper.Instance.buySound, 0.3f);
            if (panelName == "Speed")
            {
                dataManager.PlayerSpeed += 100;
                ShowMessagePanel("Speed Increased!");
            }
            else if (panelName == "Radius")
            {
                dataManager.PlayerCollectRadius += 0.5f;
                ShowMessagePanel("Player Radius Increased!");
            }
            else if (panelName == "Max Souls")
            {
                dataManager.MaxSouls *= 5;
                ShowMessagePanel("Max Souls Increased!");
            }
            else if (panelName == "+ Bonus")
            {
                //idk
                ShowMessagePanel("+ Bonus Upgraded!");
            }
            else if (panelName == "Increase Income")
            {
                dataManager.IncomeMultiplier += 1;
                ShowMessagePanel("Income Increased!");
            }

            if (panelName == "Buy Collector")
            {
                GameObject tempCollector = ResourceManager.GetCollector(unitSpawner.transform.position);
                tempCollector.GetComponent<NavMeshAgent>().speed = dataManager.MinionSpeed;
                tempCollector.GetComponent<CapsuleCollider>().radius = dataManager.CollectorRadius;
                dataManager.CollectorCount++;
                
                ShowMessagePanel("Collector count increased!");
            }
            else if (panelName == "Buy Mason")
            {
                GameObject tempMason = ResourceManager.GetMason(unitSpawner.transform.position);
                tempMason.GetComponent<NavMeshAgent>().speed = dataManager.MinionSpeed;
                tempMason.GetComponent<CapsuleCollider>().radius = dataManager.CollectorRadius;
                dataManager.MasonCount++;
                
                ShowMessagePanel("Mason count increased!");
            }
            else if (panelName == "Buy Obelisk")
            {
                SpawnObelisk();
                textNode.text = dataManager.ObeliskCount.ToString();
                ShowMessagePanel("Obelisk count increased!");
            }
            else if (panelName == "Overload")
            {
                dataManager.ObeliskChargeRate += 5;
                foreach (Obelisk tempObelisk in GameObject.FindObjectsOfType<Obelisk>())
                {
                    tempObelisk.healthDecreseRate = dataManager.ObeliskChargeRate;
                }
                
                ShowMessagePanel("Overload Upgraded!");
            }
            else if (panelName == "Minion Speed")
            {
                dataManager.MinionSpeed += 2;
                foreach (CollectorFamiliar tempCollector in GameObject.FindObjectsOfType<CollectorFamiliar>())
                {
                    tempCollector.GetComponent<NavMeshAgent>().speed = dataManager.MinionSpeed;
                }
                foreach (RepairerFamiliar tempMason in GameObject.FindObjectsOfType<RepairerFamiliar>())
                {
                    tempMason.GetComponent<NavMeshAgent>().speed = dataManager.MinionSpeed;
                }
                
                ShowMessagePanel("Minon Speed increased!");
            }
            else if (panelName == "Big Souls")
            {
                dataManager.BigSoulChance += 5;
                
                ShowMessagePanel("Big souls Upgraded!");
            }
            else if (panelName == "Collector Radius")
            {
                dataManager.CollectorRadius = dataManager.CollectorRadius + 2;
                foreach (CollectorFamiliar tempCollector in GameObject.FindObjectsOfType<CollectorFamiliar>())
                {
                    tempCollector.GetComponent<CapsuleCollider>().radius = dataManager.CollectorRadius;
                }
                foreach (RepairerFamiliar tempMason in GameObject.FindObjectsOfType<RepairerFamiliar>())
                {
                    tempMason.GetComponent<CapsuleCollider>().radius = dataManager.CollectorRadius;
                }
                
                ShowMessagePanel("Collector Radius increased!");    
            }

            GameObject.FindObjectOfType<SoulBar>().SetAll(dataManager.SoulCount, dataManager.MaxSouls);
            
        }

        SetPrices(tempType);

    }
   

    private static void SetPrices(string vendorType)
    {
        switch (vendorType)
        {
            case "player":
                for (int i = 0; i < playerPanels.Length; i++)
                {
                    upgradePanel.transform.Find(ParsePanelName(playerPanels[i])).transform.Find("Price").GetComponent<Text>().text = dataManager.GetPrice(playerPanels[i]);
                }
                break;
            case "obelisk":
                for (int i = 0; i < obeliskPanels.Length; i++)
                {
                    upgradePanel.transform.Find(ParsePanelName(obeliskPanels[i])).transform.Find("Price").GetComponent<Text>().text = dataManager.GetPrice(obeliskPanels[i]);
                }
                break;
            case "minion":
                for (int i = 0; i < minionPanels.Length; i++)
                {
                    upgradePanel.transform.Find(ParsePanelName(minionPanels[i])).transform.Find("Price").GetComponent<Text>().text = dataManager.GetPrice(minionPanels[i]);
                }
                break;
        }

    }
    
    private static string ParsePanelName(string s){
        return s.Replace(" ", "") + "Panel";
    }

    private static void SpawnObelisk()
    {
        if (obeliskPosList.Count > 0)
        {
            GameObject tempObelisk = ResourceManager.GetObelisk(obeliskPosList[0].transform.position);
            tempObelisk.GetComponent<Obelisk>().healthDecreseRate = dataManager.ObeliskChargeRate;
            obeliskPosList.Remove(obeliskPosList[0]);
            dataManager.ObeliskCount++;
        }
    }
    
    public static void PreviousItem(){
                
        selectedIndex--;
        
        if(selectedIndex < 0)
            selectedIndex = 0;
            
        //Debug.Log("PanelUp - " + vendorCache.Length + " " + selectedIndex);
        if(currentItem != null){
            currentItem = upgradePanel.transform.GetChild(selectedIndex);
            currentItem.GetComponent<Image>().color = highlightColor;
            upgradePanel.transform.GetChild(selectedIndex+1).GetComponent<Image>().color = colorCache;
            upgradePanel.transform.GetChild(selectedIndex);
        }

    }

    public static void NextItem(){
        selectedIndex++;
        
        if(selectedIndex >= vendorCache.Length)
            selectedIndex = vendorCache.Length-1;
        
        //Debug.Log("PanelDown - " + vendorCache.Length + " " + selectedIndex);
        if(currentItem != null){
            currentItem = upgradePanel.transform.GetChild(selectedIndex);
            currentItem.GetComponent<Image>().color = highlightColor;   
            upgradePanel.transform.GetChild(selectedIndex-1).GetComponent<Image>().color = colorCache;
            upgradePanel.transform.GetChild(selectedIndex);
        }
        
    }

}
