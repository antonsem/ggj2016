using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Entity : MonoBehaviour
{

    private AudioSource source;

    public void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public string Name = "DefaultEntity";
    public List<AudioClip> interactionSounds = new List<AudioClip>();

    public virtual void Interact()
    {
        if (!source.isPlaying && interactionSounds.Count > 0)
            source.PlayOneShot(interactionSounds[Random.Range(0, interactionSounds.Count)]);
    }
}
