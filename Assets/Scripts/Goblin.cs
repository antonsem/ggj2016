﻿using UnityEngine;
using System.Collections;

public class Goblin : MonoBehaviour
{

    private SkeletonAnimation anim;
    private float speed = 80;
    private float idleTime = 3;
    private float destroyTimer = 30;

    void Start()
    {
        anim = GetComponent<SkeletonAnimation>();
        anim.AnimationName = "summon";
    }

    void Update()
    {
        idleTime -= Time.deltaTime;
        destroyTimer -= Time.deltaTime;
        if (idleTime < 0)
        {
            idleTime = 1000;
            GetComponent<Rigidbody>().velocity = Vector3.right * Time.deltaTime * speed;
            anim.loop = true;
            anim.AnimationName = "walking";
        }

        if (destroyTimer < 0)
            Destroy(this.gameObject);
    }
}
