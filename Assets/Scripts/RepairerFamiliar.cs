using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Spine;

public class RepairerFamiliar : MonoBehaviour {

    public enum currentState
    {
        repair,
        idle
    };

    private SkeletonAnimation anim;
    private NavMeshAgent familiarAgent;
    private Obelisk currentTarget;
    private AudioSource source;
    private bool hasTarget;
    private float checkCooldown;
    private float soundProbability = 10;
    private float soundTimer;

    public Vector3 targetPosition;
    public currentState familiarState;
    public float checkTime = 10f;
    public float familiarRepairDistance = 2;
    public List<AudioClip> soundClips = new List<AudioClip>();

    void Awake()
    {
        familiarAgent = GetComponent<NavMeshAgent>();
        familiarState = currentState.idle;
        hasTarget = false;
        checkCooldown = Time.time + checkTime;
        anim = GetComponent<SkeletonAnimation>();
        source = GetComponent<AudioSource>();
    }

    void ManageAnimations()
    {
        if(anim.state.GetCurrent(0) == null)
        {
            anim.loop = true;
            anim.AnimationName = "idle";
        }
        //if (familiarAgent.velocity.magnitude > 0)
        //{
        //    anim.AnimationName = "run";
        //    if (familiarAgent.velocity.x < 0)
        //        transform.forward = -Vector3.forward;
        //    else
        //        transform.forward = Vector3.forward;
        //}
        //else
        //{
        //    anim.timeScale = 1;
        //    anim.AnimationName = "idle";
        //}
    }

    void Update()
    {
        soundTimer -= Time.deltaTime;
        if (soundTimer < 0)
        {
            soundTimer = 5;
            if (Random.Range(0, 100) < soundProbability)
                source.PlayOneShot(soundClips[Random.Range(0, soundClips.Count)]);
        }
        if (familiarState == currentState.idle && checkCooldown <= Time.time)
        {
            currentTarget = FindObelisk(FindObjectsOfType<Obelisk>());
            hasTarget = true;
        }

        if (hasTarget == true)
        {
            MoveToObelisk();
            if (CheckDistance())
            {
                anim.loop = false;
                anim.AnimationName = "hit";
                currentTarget.Interact();
                familiarState = currentState.idle;
                hasTarget = false;
                checkCooldown = Time.time + checkTime;
            }
        }
        ManageAnimations();
    }

    Obelisk FindObelisk(Obelisk[] currentObelisks)
    {
        float tempMinHealth = 100;
        Obelisk tempObelisk = null;
        foreach (Obelisk obelisk in currentObelisks)
        {
            if (obelisk.health < tempMinHealth)
            {
                tempMinHealth = obelisk.health;
                tempObelisk = obelisk;
            }
        }
        return tempObelisk;
    }

    void MoveToObelisk()
    {
        if (currentTarget != null)
        {
            targetPosition = new Vector3(currentTarget.transform.position.x, 1, currentTarget.transform.position.z - 1);
            familiarAgent.SetDestination(targetPosition);
            familiarState = currentState.repair;
        }
    }


    bool CheckDistance()
    {
        if (Vector3.Distance(transform.position, currentTarget.transform.position) < familiarRepairDistance) 
            return true;
        return false;
    }

}
