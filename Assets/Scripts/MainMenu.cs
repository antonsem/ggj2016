using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {


    public GameObject mainMenu;
    public GameObject instructions;
    public GameObject credits;

    public void Play()
    {
        FindObjectOfType<Cam>().startGame = true;
    }


    public void Instrucitions()
    {
        mainMenu.SetActive(false);
        instructions.SetActive(true);
        credits.SetActive(false);
    }

    public void Credits()
    {
        mainMenu.SetActive(false);
        instructions.SetActive(false);
        credits.SetActive(true);
    }

    public void Back()
    {
        mainMenu.SetActive(true);
        instructions.SetActive(false);
        credits.SetActive(false);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
