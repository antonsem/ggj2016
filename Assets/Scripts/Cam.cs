using UnityEngine;
using System.Collections;

public class Cam : MonoBehaviour
{
    public GameObject gameUI;
    public GameObject menuUI;


    private Player player;
    bool startRotate;
    bool StartRotate
    {
        get { return startRotate; }
        set
        {
            startRotate = value;
            player.canMove = !value;
        }
    }
    public bool startGame;
    Quaternion start;
    Quaternion end;
    float timer = 0;


    void Start()
    {
        player = FindObjectOfType<Player>();
        StartRotate = true;
        startGame = false;

        menuUI.SetActive(true);
        gameUI.SetActive(false);

        start.eulerAngles = new Vector3(335, 0, 0);
        end.eulerAngles = new Vector3(30, 0, 0);
    }

    void Update()
    {
        if (StartRotate == false)
        {
            float XAngle = Vector3.Angle(Vector3.forward, new Vector3(player.transform.position.x - transform.position.x, 0, player.transform.position.z - transform.position.z));
            if (XAngle < 40)
            {
                transform.LookAt(new Vector3(player.transform.position.x, 0, 0));
            }
        }
    }

    void FixedUpdate()
    {
        if (StartRotate && startGame)
        {

            menuUI.SetActive(false);
            gameUI.SetActive(true);

            timer += 0.006f;
            transform.rotation = Quaternion.Lerp(start, end, timer);
            if (timer >= 1)
            {
                StartRotate = false;
            }
        }

    }
}
