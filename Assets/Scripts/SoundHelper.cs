﻿using UnityEngine;
using System.Collections;

public class SoundHelper : Singleton<SoundHelper>
{

    private AudioSource source;

    public AudioClip buySound;

    public void PlaySound(AudioClip clip, float vol = 1, float pitchDif = 0)
    {
        source.volume = vol;
        source.pitch = Random.Range(1f - pitchDif, 1f);
        source.PlayOneShot(clip);
    }

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

}
