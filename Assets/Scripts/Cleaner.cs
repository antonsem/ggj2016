﻿using UnityEngine;
using System.Collections;

public class Cleaner : MonoBehaviour
{

    private Player player;

    void OnColliderEnter(Collider col)
    {
        player.clearList.Add(col.GetComponent<Entity>());
    }

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<Player>();
    }
}
