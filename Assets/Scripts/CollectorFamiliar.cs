using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectorFamiliar : MonoBehaviour
{

    public enum currentState
    {
        follow,
        idle
    };

    private SkeletonAnimation anim;
    private NavMeshAgent familiarAgent;
    private Transform currentTarget;
    private AudioSource source;
    private bool hasTarget;
    private float timeoutCooldown;
    private float soundProbability = 10;
    private float soundTimer;

    public Vector3 targetPosition;
    public currentState familiarState;
    public float timeout = 3f;
    public List<AudioClip> soundClips = new List<AudioClip>();


    void Awake()
    {
        familiarAgent = GetComponent<NavMeshAgent>();
        familiarState = currentState.idle;
        hasTarget = false;
        anim = GetComponent<SkeletonAnimation>();
        source = GetComponent<AudioSource>();
    }

    void ManageAnimations()
    {
        if (familiarAgent.velocity.magnitude > 0)
        {
            anim.AnimationName = "walking";
            if (familiarAgent.velocity.x < 0)
                transform.forward = -Vector3.forward;
            else
                transform.forward = Vector3.forward;
        }
        else
        {
            anim.timeScale = 1;
            anim.AnimationName = "idle";
        }
    }

    void Update()
    {
        soundTimer -= Time.deltaTime;
        if(soundTimer < 0)
        {
            soundTimer = 5;
            if (Random.Range(0, 100) < soundProbability)
                source.PlayOneShot(soundClips[Random.Range(0, soundClips.Count)]);
        }
        if (FindObjectsOfType<Soul>() != null && familiarState == currentState.idle)
        {
            currentTarget = FindMana();
            hasTarget = true;
            timeoutCooldown = Time.time + timeout;
        }

        if (hasTarget == true && currentTarget != null)
        {
            MoveToMana();
        }

        if (timeoutCooldown <= Time.time)
        {
            Timeout();
            Debug.Log("TIMEOUT!!! for " + transform.name);
        }
        ManageAnimations();
    }

    Transform FindMana()
    {
        if (FindObjectsOfType<Soul>().Length > 0)
        {
            Transform tempMana = FindObjectsOfType<Soul>()[Random.Range(0, FindObjectsOfType<Soul>().Length - 1)].transform;
            return tempMana;
        }
        return null;
    }

    void MoveToMana()
    {
        if (currentTarget != null)
        {
            targetPosition = new Vector3(currentTarget.position.x, 1, currentTarget.transform.position.z);
            familiarAgent.SetDestination(targetPosition);
            familiarState = currentState.follow;

        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Soul>() == null) return;

        other.GetComponent<Soul>().CollectorInteract();

        familiarState = currentState.idle;
        hasTarget = false;
    }

    void Timeout()
    {
        familiarState = currentState.idle;
        hasTarget = false;
        currentTarget = null;
    }

}
