using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class StoryView : MonoBehaviour {

    public int totalGoblins = 0;
    public Text storyView;

    public string storyLog = "";

    public Dictionary<int, string> milestones;

    void Start()
    {
        milestones = new Dictionary<int, string>();
        milestones.Add(1, "You summoned a goblin!");

        // TEST
        milestones.Add(2, "You summoned a goblin!");
        milestones.Add(3, "You summoned a goblin!");
        milestones.Add(4, "You summoned a goblin!");
        milestones.Add(6, "You summoned a goblin!");
        milestones.Add(7, "You summoned a goblin!");
        // TEST


        milestones.Add(5, "You summoned 5 goblins!");
        milestones.Add(10, "You summoned 10 goblins!");
        milestones.Add(25, "You summoned 25 goblins!");
        milestones.Add(50, "You summoned 50 goblins!");
        milestones.Add(100, "You summoned 100 goblins, summoning is almost complete!");
    }

    public void AddGoblin()
    {
        totalGoblins++;
        if (milestones.ContainsKey(totalGoblins))
        {
            storyView.text = milestones[totalGoblins] + storyLog;

            storyLog += "\n" + milestones[totalGoblins];
        }
    }


}
