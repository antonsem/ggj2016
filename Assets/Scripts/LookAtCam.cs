﻿using UnityEngine;
using System.Collections;

public class LookAtCam : MonoBehaviour
{

    Transform camTransform;

    void Start()
    {
        camTransform = Camera.main.transform;
    }

    void Update()
    {
        transform.LookAt(camTransform.position, Vector3.up);
    }
}
