using UnityEngine;
using System.Collections;
public class Vendor : Entity
{
    public enum vendorType
    {
        player,
        minion,
        obelisk
    };
    public vendorType thisType;
    public override void Interact()
    {
        base.Interact();
        if (thisType == vendorType.minion)
        {
            UIManager.ToggleUpgradePanel("minion");
        }
        else if (thisType == vendorType.obelisk)
        {
            UIManager.ToggleUpgradePanel("obelisk");
        }
        else if (thisType == vendorType.player)
        {
            UIManager.ToggleUpgradePanel("player");
        }
    }
}