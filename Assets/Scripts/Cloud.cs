﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour
{
    public float speed = 0.01f;

    void Update()
    {
        transform.Translate(25 * Time.deltaTime * speed, 0, 0);
        if (Mathf.Abs(transform.position.x) > 25)
            speed *= -1;
    }
}
