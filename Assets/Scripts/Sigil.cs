﻿using UnityEngine;
using System.Collections;

public class Sigil : MonoBehaviour
{
    private Material glow;
    private Material shiny;
    private float timer = 0;

    void Start()
    {
        glow = transform.FindChild("Glow").GetComponent<Renderer>().material;
        shiny = transform.FindChild("Shiny").GetComponent<Renderer>().material;
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            timer = 1;
            StartCoroutine(FadeTo(glow, 1 - Random.Range(0f, 1f), 1));
            StartCoroutine(FadeTo(shiny, 1 - Random.Range(0f, 1f), 1));
        }
    }

    IEnumerator FadeTo(Material mat, float aValue, float aTime)
    {
        float alpha = mat.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            mat.color = newColor;
            yield return null;
        }
    }
}
