using UnityEngine;
using System.Collections;

public class ResourceManager
{
    public static GameObject GetSoul(Vector3 pos)
    {
        return GameObject.Instantiate(Resources.Load("Prefabs/Soul"), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject GetBigSoul(Vector3 pos)
    {
        return GameObject.Instantiate(Resources.Load("Prefabs/BigSoul"), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject GetPanelItem(Vector3 pos)
    {
        return GameObject.Instantiate(Resources.Load("Prefabs/UpgradePanelItem"), pos, Quaternion.identity) as GameObject;
    }
    
    public static GameObject GetMessageItem(Vector3 pos)
    {
        return GameObject.Instantiate(Resources.Load("Prefabs/MessagePanelItem"), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject GetCollector(Vector3 pos)
    {
        return GameObject.Instantiate(Resources.Load("Prefabs/Collector"), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject GetMason(Vector3 pos)
    {
        return GameObject.Instantiate(Resources.Load("Prefabs/Mason"), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject GetObelisk(Vector3 pos)
    {
        return GameObject.Instantiate(Resources.Load("Prefabs/Obelisk"), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject GetGoblin(Vector3 pos)
    {
        return GameObject.Instantiate(Resources.Load("Prefabs/Goblin"), pos, Quaternion.identity) as GameObject;
    }

    public static GameObject GetFlare(Vector3 pos)
    {
        return GameObject.Instantiate(Resources.Load("Prefabs/Flare"), pos, Quaternion.identity) as GameObject;
    }
}
