using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    Rigidbody rigid;
    private float moveX;
    private float moveY;
    private DataManager dataManager;
    private List<Entity> interactables = new List<Entity>();
    private List<AudioClip> clipList = new List<AudioClip>();
    private SkeletonAnimation anim;
    private AudioSource source;

    public List<Entity> clearList = new List<Entity>();
    public float speed;
    public bool canMove = false;
    public AudioClip footStep;

    void Awake()
    {
        UIManager.Init();
    }

    void Start()
    {
        dataManager = DataManager.Instance;
        rigid = GetComponent<Rigidbody>();
        anim = GetComponent<SkeletonAnimation>();
        source = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider col)
    {
        Entity _entity = col.GetComponent<Entity>();
        if (_entity != null && !(_entity is Soul))
        {
            interactables.Add(_entity);
        }
    }

    void OnTriggerStay(Collider col)
    {
        Entity _entity = col.GetComponent<Entity>();
        if (_entity != null && _entity is Soul)
        {
            _entity.Interact();
        }
    }

    void OnTriggerExit(Collider col)
    {
        Entity _entity = col.GetComponent<Entity>();
        if (_entity != null && interactables.Contains(_entity))
        {
            if (_entity is Vendor)
            {
                canMove = true;
                UIManager.TurnOffUpgradePanel();
            }
            interactables.Remove(_entity);
        }
    }

    void Move()
    {
        moveX = Input.GetAxisRaw("Horizontal");
        moveY = Input.GetAxisRaw("Vertical");

        rigid.velocity = (Camera.main.transform.right * moveX + Camera.main.transform.forward * moveY).normalized * Time.deltaTime * speed;
        //rigid.velocity = new Vector3(moveX, 0, moveY).normalized * Time.deltaTime * speed;
    }

    void Actions()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (Entity e in interactables)
            {
                e.Interact();
            }
        }
        
        if (Input.GetKeyDown(KeyCode.Escape)){
            UIManager.CloseUpgradePanel();
        }
        
        if(Input.GetKeyDown(KeyCode.DownArrow) && UIManager.isPanelOpened || 
        Input.GetKeyDown(KeyCode.S) && UIManager.isPanelOpened){
            UIManager.NextItem();
        }else if(Input.GetKeyDown(KeyCode.UpArrow) && UIManager.isPanelOpened || 
        Input.GetKeyDown(KeyCode.W) && UIManager.isPanelOpened ){
            UIManager.PreviousItem();
        }
        
        if(Input.GetKeyDown(KeyCode.Return) && UIManager.isPanelOpened || 
        
        Input.GetKeyDown(KeyCode.E) && UIManager.isPanelOpened){
            UIManager.IncreaseUpgrade(UIManager.currentItem.transform.Find("Explanation").GetComponent<Text>().text);
            Debug.Log("something");
        }
    }

    void Clear()
    {
        for (int i = 0; i < clearList.Count; i++)
        {
            if (interactables.Contains(clearList[i]))
                interactables.Remove(clearList[i]);

            if (clearList[i] is Soul)
                ResourceManager.GetFlare(clearList[i].transform.position);

            Destroy(clearList[i].gameObject);
        }
        clearList.Clear();
    }

    //IEnumerator PlaySound(AudioClip clip, float addToQue)
    //{
    //}

    void ManageAnimations()
    {
        if (rigid.velocity.magnitude > 0.01f)
        {
            anim.AnimationName = "walking";
            anim.timeScale = speed / 275;
            transform.right = rigid.velocity.x > 0 ? Vector3.right : -Vector3.right;
            if (!source.isPlaying)
            {
                source.clip = footStep;
                source.pitch = Random.Range(0.8f, 1f);
                source.PlayDelayed(0.1f * 275 / speed);
            }
        }
        else
        {
            anim.AnimationName = "idle";
            anim.timeScale = 1;
        }
    }

    void Update()
    {
        if (canMove)
            Move();
        Actions();
        ManageAnimations();
        Clear();
    }
}
