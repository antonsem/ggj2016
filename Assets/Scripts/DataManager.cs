using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DataManager : Singleton<DataManager>
{
    public Transform goblinSpawner;

    private int summonCount = 50;

    private int soulCount = 0;
    public int SoulCount
    {
        get { return soulCount; }
        set
        {
            if (value + incomeMultiplier >= maxSouls)
            {
                soulCount = maxSouls;
            }
            else if (value < maxSouls - incomeMultiplier)
            {
                soulCount = value + incomeMultiplier;
            }
            UIManager.SoulAmount = soulCount;
            FindObjectOfType<SoulBar>().SetSoulsBar(soulCount);
        }
    }

    private int collectorCount = 0;
    public int CollectorCount
    {
        get { return collectorCount; }
        set
        {
            collectorCount = value;
            UIManager.CollectorCount = collectorCount;
        }
    }

    private int masonCount = 0;
    public int MasonCount
    {
        get { return masonCount; }
        set
        {
            masonCount = value;
            UIManager.MasonCount = masonCount;
        }
    }

    private int incomeMultiplier = 0;
    public int IncomeMultiplier
    {
        get { return incomeMultiplier; }
        set
        {
            incomeMultiplier = value;
            UIManager.IncomeCount = incomeMultiplier;
        }
    }

    private int obeliskCount = 0;
    public int ObeliskCount
    {
        get { return obeliskCount; }
        set
        {
            obeliskCount = value;
            UIManager.ObeliskCount = obeliskCount;
        }
    }

    private int maxSouls = 100;
    public int MaxSouls
    {
        get { return maxSouls; }
        set
        {
            maxSouls = value;
            FindObjectOfType<SoulBar>().SetMaxSouls(maxSouls);
        }
    }

    private int obeliskChargeRate = 5;
    public int ObeliskChargeRate
    {
        get { return obeliskChargeRate; }
        set
        {
            obeliskChargeRate = value;
        }
    }

    private int playerSpeed = 300;
    public int PlayerSpeed
    {
        get { return playerSpeed; }
        set
        {
            playerSpeed = value;
            FindObjectOfType<Player>().speed = playerSpeed;
        }
    }

    private float playerCollectRadius = 2f;

    public float PlayerCollectRadius
    {
        get { return playerCollectRadius; }
        set
        {
            playerCollectRadius = value;
            FindObjectOfType<Player>().GetComponent<SphereCollider>().radius = value;
        }
    }

    private int playerBonus = 0;

    public int PlayerBonus
    {
        get { return playerBonus; }
        set
        {
            playerBonus = value;
        }
    }

    private int minionSpeed = 6;

    public int MinionSpeed
    {
        get { return minionSpeed; }
        set
        {
            minionSpeed = value;
        }
    }

    private int collectorRadius = 1;

    public int CollectorRadius
    {
        get { return collectorRadius; }
        set
        {
            collectorCount = value;
        }
    }

    private int bigSoulChance = 5;
    public int BigSoulChance
    {
        get
        {
            return bigSoulChance;
        }
        set
        {
            bigSoulChance = value;
        }
    }




    // PRICES


    public float collectorPrice
    {
        get
        {
            return 10 * (collectorCount + 1) * (collectorCount + 1);
        }
    }

    public float masonPrice
    {
        get
        {
            return 20 * (masonCount + 1) * (masonCount + 1);
        }
    }

    public float maxSoulPrice
    {
        get
        {
            return maxSouls - 5;
        }
    }

    public float obeliskPrice
    {
        get
        {
            return 100 * (obeliskCount + 1) * (obeliskCount + 1);
        }
    }

    public float incomePrice
    {
        get
        {
            return 200 * (incomeMultiplier + 1) * (incomeMultiplier + 1);
        }
    }

    public float obeliskChargeRatePrice
    {
        get
        {
            return 20 * (obeliskChargeRate - 4) * (obeliskChargeRate - 4);
        }
    }

    public int playerSpeedUpgradePrice
    {
        get
        {
            return 30 * (playerSpeed / 100 - 2);
        }
    }

    public float playerCollectRadiusPrice
    {
        get
        {
            return 20 * (playerCollectRadius * 2);
        }
    }

    public int playerBonusPrice
    {
        get
        {
            return 10 * (playerBonus + 1);
        }
    }

    public int minionSpeedPrice
    {
        get
        {
            return 15 * (minionSpeed - 2);
        }
    }

    public int collectorRadiusPrice
    {
        get
        {
            return 20 * (collectorRadius) * (collectorRadius);
        }
    }

    public int bigSoulsPrice
    {
        get
        {
            return 5 * (bigSoulChance);
        }
    }

    public void DecreaseSouls(int soulsToDecrease)
    {
        soulCount = soulCount - soulsToDecrease;
    }

    public string GetPrice(string priceName)
    {
        if (priceName == "Buy Collector")
            return collectorPrice.ToString();
        else if (priceName == "Buy Mason")
            return masonPrice.ToString();
        else if (priceName == "Buy Obelisk")
            return obeliskPrice.ToString();
        else if (priceName == "Increase Income")
            return incomePrice.ToString();
        else if (priceName == "Max Souls")
            return maxSoulPrice.ToString();
        else if (priceName == "Overload")
            return obeliskChargeRatePrice.ToString();
        else if (priceName == "Speed")
            return playerSpeedUpgradePrice.ToString();
        else if (priceName == "Radius")
            return playerCollectRadiusPrice.ToString();
        else if (priceName == "Minion Speed")
            return minionSpeedPrice.ToString();
        else if (priceName == "Collector Radius")
            return collectorRadiusPrice.ToString();
        else if (priceName == "Big Souls")
            return bigSoulsPrice.ToString();
        return null;
    }

    public IEnumerator FadeTo(Image img, float aValue, float aTime)
    {
        float alpha = img.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            img.color = newColor;
            yield return null;

        }
    }

    public void CountSummonSouls(int count)
    {
        summonCount -= count;
        if (summonCount <= 0)
        {
            GameObject go = ResourceManager.GetGoblin(goblinSpawner.position + Vector3.forward * Random.Range(-1f, 1f));
            go.transform.localScale *= Random.Range(0.6f, 1.2f);
            FindObjectOfType<StoryView>().AddGoblin();
            summonCount = 50;
        }
    }

}
