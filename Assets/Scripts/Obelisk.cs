using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Obelisk : Entity
{
    private GameObject spawner;
    private Image healthBar;
    private SpriteRenderer sprite;
    public float health = 100;
    private float spawnTimer = 0;
    private DataManager dataManager;

    public float healthDecreseRate = 5;

    public List<Sprite> stateList = new List<Sprite>();

    void Start()
    {
        base.Start();
        spawner = transform.FindChild("Spawner").gameObject;
        healthBar = transform.FindChild("HealthBar").GetComponentInChildren<Image>();
        sprite = GetComponent<SpriteRenderer>();
        dataManager = FindObjectOfType<DataManager>();
    }

    public override void Interact()
    {
        base.Interact();
        for (int i = 0; i < (int)(100 / health); i++)
        {
            ResourceManager.GetSoul(spawner.transform.position + Vector3.up * 0.2f * i);
        }

        if (Random.Range(0, 100) <= dataManager.BigSoulChance)
        {
            ResourceManager.GetBigSoul(spawner.transform.position + Vector3.up * 0.2f);
        }

        health += health < 100 ? 10 : 0;
    }

    void Update()
    {
        health -= health < 10 ? 0 : Time.deltaTime * healthDecreseRate;
        healthBar.rectTransform.sizeDelta = new Vector2(1200 * (health / 100), 700);
        ChangeSprite();
    }

    private void ChangeSprite()
    {
        if (health > 96)
            sprite.sprite = stateList[0];
        else if (health > 80)
            sprite.sprite = stateList[1];
        else if (health > 64)
            sprite.sprite = stateList[2];
        else if (health > 48)
            sprite.sprite = stateList[3];
        else if (health > 32)
            sprite.sprite = stateList[4];
        else if(health > 16)
            sprite.sprite = stateList[5];

    }
}
